package edu.uchicago.gerber.repos;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import edu.uchicago.gerber.models.FavoriteItem;
import io.quarkus.runtime.StartupEvent;
import org.bson.Document;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class FavsRepository {


    @Inject
    MongoClient mongoClient;


    private Gson gson = new Gson();

    public static final int PAGE_SIZE = 20;

    public static  final String DEFAULT_EMAIL = "default@default.com";


    //this will get fired when the quarkus microservice starts
    void onStart(@Observes StartupEvent ev) {

        long collectionSize = getCollection().countDocuments();
        if (collectionSize > 0) return;

        Faker faker = new Faker();
        getCollection().insertMany(
                Stream.generate(
                        () -> new FavoriteItem(
                                UUID.randomUUID().toString(),
                                DEFAULT_EMAIL,
                                faker.beer().name(),
                                faker.company().name(),
                                faker.company().buzzword(),
                                faker.beer().name(),
                                faker.internet().domainName())
                )
                        .peek(fav -> System.out.println(fav))
                        .map(fav -> item2doc(fav))
                        .limit(100).collect(Collectors.toList())

        );


    }

    public FavoriteItem add(FavoriteItem favoriteItem) {
        getCollection().insertOne(item2doc(favoriteItem));
        return  favoriteItem;
    }

    public FavoriteItem get(String userEmail, String id) {
        BasicDBObject query = new BasicDBObject();
        query.put("userEmail", userEmail);
        query.put("id", id);

        FindIterable<Document> documents = getCollection().find(query);


        List<FavoriteItem> items = new ArrayList<>();
        for (Document document : documents) {
            items.add(doc2item(document));
        }

        //this will produce a 404 not found
        if (items.size() != 1) return null;

        return items.get(0);


    }


    public FavoriteItem delete( String userEmail, String id ) {
        BasicDBObject query = new BasicDBObject();
        query.put("userEmail", userEmail);
        query.put("id", id);

        FindIterable<Document> documents = getCollection().find(query);

        Document firstDocument = null;
        for (Document document : documents) {
            firstDocument = document;
            break;
        }
        getCollection().deleteOne(firstDocument);
        return doc2item(firstDocument);
    }

    //https://www.technicalkeeda.com/java-mongodb-tutorials/java-mongodb-driver-3-3-0-pagination-example
    public List<FavoriteItem> paged(String userEmail, int page){
        BasicDBObject query = new BasicDBObject();
        query.put("userEmail", userEmail);
        List<FavoriteItem> favs = new ArrayList<>();
        try {
            MongoCursor<Document> cursor =
                    getCollection().find(query).skip(PAGE_SIZE * (page - 1)).limit(PAGE_SIZE).iterator();
            while (cursor.hasNext()) {
                Document document = cursor.next();
                favs.add(doc2item(document));
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return favs;
    }


    //define the collection
    private MongoCollection getCollection() {
        return mongoClient.getDatabase("favs_db").getCollection("favs_collection");
    }

    //local transform ops
    private FavoriteItem doc2item(Document document) {
        if (document != null && !document.isEmpty()) {
            return gson.fromJson(document.toJson(), FavoriteItem.class);
        }
        return null;

    }

    private Document item2doc(FavoriteItem item) {
        if (item != null) {
            String json = gson.toJson(item);
            return Document.parse(json);
        }
        return null;
    }




}
