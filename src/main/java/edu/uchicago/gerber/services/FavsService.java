package edu.uchicago.gerber.services;

import edu.uchicago.gerber.models.FavoriteItem;
import edu.uchicago.gerber.repos.FavsRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.NotSupportedException;


import java.util.List;

@ApplicationScoped
public class FavsService {

    @Inject
    FavsRepository favsRepository;


    public FavoriteItem add(FavoriteItem favoriteItem) {
        //check for dup before adding record.
        FavoriteItem checkDup = favsRepository.get(favoriteItem.getUserEmail(), favoriteItem.getId());
        if (null != checkDup){
            throw new NotSupportedException("The FavoriteItem with id " + favoriteItem.getId() + " already exists");
        }
        return favsRepository.add(favoriteItem);
    }

    public FavoriteItem get(String userEmail, String id) {
        FavoriteItem item = favsRepository.get(userEmail, id );
        if (null == item){
            throw new NotFoundException("The FavoriteItem with id " + id + " was not found");
        }
        return item;
    }


    public FavoriteItem delete(String userEmail, String id) {
        FavoriteItem item = favsRepository.get(userEmail, id);
        if (null == item){
            throw new NotFoundException("The FavoriteItem with id " + id + " was not found");
        }
        return favsRepository.delete(id, userEmail);
    }

    public List<FavoriteItem> paged(String userEmail, int page){
        return favsRepository.paged(userEmail, page);
    }

}
