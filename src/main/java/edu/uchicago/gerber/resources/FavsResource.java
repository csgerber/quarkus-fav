package edu.uchicago.gerber.resources;

import edu.uchicago.gerber.models.FavoriteItem;
import edu.uchicago.gerber.services.FavsService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/favs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FavsResource {


    //add : Fav
    //get(userEmail, id) : Fav
    //delete(userEmail, id) : Fav
    //paged(userEmail, page) : List<Fav>

    @Inject
    FavsService favsService;


    @POST
    public FavoriteItem add(FavoriteItem favoriteItem){
        return favsService.add(favoriteItem);
    }

    @GET
    @Path("{userEmail}/{id}")
    public FavoriteItem get(@PathParam("userEmail") String userEmail, @PathParam("id") String id) {
        return favsService.get(userEmail, id);
    }

    @DELETE
    @Path("{userEmail}/{id}")
    public FavoriteItem delete(@PathParam("userEmail") String userEmail, @PathParam("id") String id) {
        return favsService.delete(userEmail, id);
    }

    @GET
    @Path("/paged/{userEmail}/{page}")
    public List<FavoriteItem> paged(@PathParam("userEmail") String userEmail, @PathParam("page") int page){
        return  favsService.paged(userEmail, page);
    }




}